﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRandomRangePositionTrigger : TriggerInterface
{

    public RectBounds Bounds;
    public GameObject Object;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Vector2 topLeft = Bounds.GetTopLeft();
        Vector2 bottomRight = Bounds.GetBottomRight();
        Object.transform.position=new Vector3(
            Random.Range(topLeft.x, bottomRight.x),
            Random.Range(topLeft.y, bottomRight.y),
            Object.transform.position.z
        );
    }
}
