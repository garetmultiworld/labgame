﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectTrigger : TriggerInterface
{

    public GameObject TheObject;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (TheObject != null)
        {
            Destroy(TheObject);
        }
    }
}
