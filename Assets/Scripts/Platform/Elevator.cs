﻿using MoreMountains.Tools;
using UnityEngine;

public class Elevator: MMPathMovement
{

    public bool BlocksPlayerMovement;
    public TriggerInterface OnMoveTrigger;
    public TriggerInterface OnArriveTrigger;

    public void SetCanMove(bool newCanMove)
    {
        CanMove = newCanMove;
        CheckPlayerMovement();
    }

    public void MoveForwards()
    {
        if (CycleOption== CycleOptions.StopAtBounds && _currentIndex == PathElements.Count-1)
        {
            return;
        }
        if (OnMoveTrigger != null)
        {
            OnMoveTrigger.Trigger();
        }
        _direction = 1;
        _previousPoint = _currentPoint.Current;
        _currentPoint.MoveNext();
        SetCanMove(true);
    }

    public void MoveBackwards()
    {
        if (CycleOption == CycleOptions.StopAtBounds && _currentIndex == 0)
        {
            return;
        }
        if (OnMoveTrigger != null)
        {
            OnMoveTrigger.Trigger();
        }
        _direction = -1;
        _previousPoint = _currentPoint.Current;
        _currentPoint.MoveNext();
        SetCanMove(true);
    }

    public void MoveToIndex(int index)
    {
        if (index == _currentIndex)
        {
            return;
        }
        if (OnMoveTrigger != null)
        {
            OnMoveTrigger.Trigger();
        }
        if (index< _currentIndex)
        {
            _direction = -1;
        }
        else
        {
            _direction = 1;
        }
        _previousPoint = _currentPoint.Current;
        _currentPoint.MoveNext();
        SetTargetIndex(index);
        SetCanMove(true);
    }

    public void CheckPlayerMovement()
    {
        if (BlocksPlayerMovement)
        {
            if (CanMove)
            {
                PlayerEvent.Trigger(PlayerEventTypes.DisableHorizontalMovement, null, 0.0f);
                PlayerEvent.Trigger(PlayerEventTypes.DisableJump, null, 0.0f);
            }
            else
            {
                PlayerEvent.Trigger(PlayerEventTypes.EnableHorizontalMovement, null, 0.0f);
                PlayerEvent.Trigger(PlayerEventTypes.EnableJump, null, 0.0f);
            }
        }
    }

    protected override void ExecuteUpdate()
	{
        if (_changedPoint)
        {
            _changedPoint = false;
            if (OnArriveTrigger != null)
            {
                OnArriveTrigger.Trigger();
            }
            CheckPlayerMovement();
        }
        base.ExecuteUpdate();
	}

}
