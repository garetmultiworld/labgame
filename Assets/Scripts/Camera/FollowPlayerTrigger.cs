﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerTrigger : TriggerInterface
{

    public CinemachineCameraController cinemachineCameraController;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineCameraController.StartFollowing();
    }

}
