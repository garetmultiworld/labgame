﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FramingTransposerTrigger : TriggerInterface
{

    public CinemachineVirtualCamera TheCamera;

    [Range(0, 2)]
    [Tooltip("Camera will not move horizontally if the target is within this range of the position.")]
    public float m_DeadZoneWidth;
    [Range(0, 2)]
    [Tooltip("Camera will not move vertically if the target is within this range of the position.")]
    public float m_DeadZoneHeight;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        CinemachineFramingTransposer framingTransposer = TheCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
        framingTransposer.m_DeadZoneWidth = m_DeadZoneWidth;
        framingTransposer.m_DeadZoneHeight = m_DeadZoneHeight;
    }
}
