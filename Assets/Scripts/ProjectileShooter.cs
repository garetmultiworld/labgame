﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{

    public Transform Target=null;

    public MMSimpleObjectPooler ProjectilePool;
    public float ShootInterval=1f;
    public int AmountPerGroup = 1;

    protected bool Shooting=false;
    protected float IntervalTimer = 0f;
    protected Vector3 Destination;
    protected int AmountShotThisGroup=0;

    public void GetTargetFromBrain(AIBrainInterface brain)
    {
        Target = brain.Target;
        StartShooting();
    }

    public void SetTarget(Transform target)
    {
        Target = target;
    }

    public void StartShooting()
    {
        AmountShotThisGroup = 0;
        Shooting = true;
        IntervalTimer = ShootInterval;
        RecalculateDestination();
    }

    public void StopShooting()
    {
        Shooting = false;
    }

    void RecalculateDestination()
    {
        Destination = new Vector3(Target.position.x, Target.position.y, 0);
    }

    void Update()
    {
        if (!Shooting|| Target==null)
        {
            return;
        }
        Projectile projectile;
        GameObject gameObject;
        IntervalTimer += Time.deltaTime;
        while (IntervalTimer >= ShootInterval)
        {
            Vector3 Origin = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y,0);
            if (AmountShotThisGroup>= AmountPerGroup)
            {
                RecalculateDestination();
                AmountShotThisGroup = 0;
            }
            AmountShotThisGroup++;
            IntervalTimer -= ShootInterval;
            gameObject = ProjectilePool.GetPooledGameObject();
            projectile = gameObject.GetComponent<Projectile>();
            Vector3 direction = Destination - Origin;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            float offset = 0;
            gameObject.transform.position = Origin;
            gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle + offset));
            direction = direction.normalized;
            projectile.Direction = direction;
            gameObject.SetActive(true);
        }
    }

}
