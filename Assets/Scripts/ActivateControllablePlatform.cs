﻿using UnityEngine;
using System.Collections;


public class ActivateControllablePlatform : MonoBehaviour
{
    private bool inPlatform = false;
    // UserControl PlatformScript;
    GameObject player;


    void Start()
    {
        // PlatformScript = GetComponent<UserControl>(); //platform control 
        player = GameObject.FindWithTag("Player");
        // guiObj.SetActive(false);
    }

    // Update is called once per frame
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && inPlatform == false)
        {
            if (Input.GetKey(KeyCode.E))
            {
                player.transform.parent = gameObject.transform;              
                // PlatformScript.enabled = true;
                player.SetActive(false);
                inPlatform = true;
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

        }
    }
    void Update()
        {
            if (inPlatform == true   && Input.GetKey(KeyCode.E))
        {
            // PlatformScript.enabled = false;
            player.SetActive(true);
            player.transform.parent = null;
            inPlatform = false;
        }
    }
}