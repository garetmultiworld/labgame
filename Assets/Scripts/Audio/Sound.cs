﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{

    public string name;
    public AudioClip clip;
    public AudioMixerGroup audioMixerGroup;
    public bool loop;
    [Range(0f,1f)]
    public float volume=1f;
    [Range(-3f, 3f)]
    public float pitch=1f;
    [Range(-1f, 1f)]
    public float pan = 0f;
    [Range(0f, 1.1f)]
    public float reverb = 1.0f;
    [HideInInspector]
    public List<AudioSource> ActiveSounds;

    public void MultiplyActiveVolume(float newVolume)
    {
        ActiveSounds.RemoveAll(item => item == null);
        foreach (AudioSource audio in ActiveSounds)
        {
            audio.volume *= newVolume;
        }
    }

    public void SetMute(bool newMute)
    {
        ActiveSounds.RemoveAll(item => item == null);
        foreach (AudioSource audio in ActiveSounds)
        {
            audio.mute = newMute;
        }
    }


}
