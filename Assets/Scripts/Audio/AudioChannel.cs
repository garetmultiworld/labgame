﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioChannel
{

    public string name;
    public Sound[] sounds;
    [Range(0f, 1f)]
    public float volume = 1f;

    [HideInInspector]
    public MusicPlayer musicPlayer;

}
