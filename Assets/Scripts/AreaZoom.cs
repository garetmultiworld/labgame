﻿using Cinemachine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaZoom : MonoBehaviour
{

    public CinemachineVirtualCamera cinemachineVirtualCamera;
    [Header("On Enter")]
    public bool ZoomOnEnter=true;
    public float Speed;
    public float TargetZoom;
    public AnimationCurve AnimCurve;

    [Header("On Exit")]
    public bool ZoomOnExit;
    public float OnExitSpeed;
    public float OnExitTargetZoom;
    public AnimationCurve OnExitAnimCurve;

    private float CurrentSpeed;
    private float CurrentTargetZoom;
    private AnimationCurve CurrentAnimCurve;

    private float StartingZoom;

    private bool IsAnimating;

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        if(
            (cinemachineVirtualCamera.m_Lens.OrthographicSize-0.1f)<= CurrentTargetZoom &&
            (cinemachineVirtualCamera.m_Lens.OrthographicSize + 0.1f) >= CurrentTargetZoom
        )
        {
            cinemachineVirtualCamera.m_Lens.OrthographicSize = CurrentTargetZoom;
            IsAnimating = false;
        }
        else
        {
            float timeLapse;
            if(CurrentTargetZoom > StartingZoom)
            {
                timeLapse = cinemachineVirtualCamera.m_Lens.OrthographicSize / CurrentTargetZoom;
                cinemachineVirtualCamera.m_Lens.OrthographicSize += CurrentSpeed * CurrentAnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                timeLapse = CurrentTargetZoom / cinemachineVirtualCamera.m_Lens.OrthographicSize;
                cinemachineVirtualCamera.m_Lens.OrthographicSize -= CurrentSpeed * CurrentAnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player")&& ZoomOnEnter)
        {
            CurrentSpeed = Speed;
            CurrentTargetZoom = TargetZoom;
            CurrentAnimCurve = AnimCurve;
            StartingZoom = cinemachineVirtualCamera.m_Lens.OrthographicSize;
            IsAnimating = true;
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.layer== LayerMask.NameToLayer("Player")&& ZoomOnExit)
        {
            CurrentSpeed = OnExitSpeed;
            CurrentTargetZoom = OnExitTargetZoom;
            CurrentAnimCurve = OnExitAnimCurve;
            StartingZoom = cinemachineVirtualCamera.m_Lens.OrthographicSize;
            IsAnimating = true;
        }
    }

}
