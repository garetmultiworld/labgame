﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlayerEvent : TriggerInterface
{

    public PlayerEventTypes type;
    public AnimationParameterTrigger animationParameterTrigger;
    public float NewValue;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        PlayerEvent.Trigger(type, animationParameterTrigger, NewValue);
    }

    public override void Cancel()
    {
    }

}
