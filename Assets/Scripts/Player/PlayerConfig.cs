﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConfig : MMSingleton<PlayerConfig>
{

    public float EnvironmentDetectionRadius = 2;

    public static PlayerConfig GetInstance()
    {
        return _instance;
    }

}
