﻿using MoreMountains.CorgiEngine;
using MoreMountains.InventoryEngine;
using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLAB : MonoBehaviour, MMEventListener<PlayerEvent>
{

    public InventoryItem BasicGun;
    public InventoryItem BasicGunAmmo;

    void Start()
    {
        MMEventManager.AddListener<PlayerEvent>(this);
    }

    void OnDestroy()
    {
        MMEventManager.RemoveListener<PlayerEvent>(this);
    }

    public void ActivarDash()
    {
        GetComponent<CharacterDash>().enabled = true;
    }

    public void ActivarWalljump()
    {
        GetComponent<CharacterWalljump>().enabled = true;
    }

    public void DesactivarDoublejump()
    {
        GetComponent<CharacterJump>().NumberOfJumps = 1;
    }

    public void ActivarDoublejump()
    {
        GetComponent<CharacterJump>().NumberOfJumps = 2;
    }

    public void ActivarBasicGun()
    {
        MMInventoryEvent.Trigger(MMInventoryEventType.Pick, null, "MainInventory", BasicGun, 1, 0);
        MMInventoryEvent.Trigger(MMInventoryEventType.Pick, null, "MainInventory", BasicGunAmmo, 100, 0);
    }

    public void DisableHorizontalMovement()
    {
        gameObject.GetComponent<CharacterHorizontalMovement>().enabled = false;
    }

    public void EnableHorizontalMovement()
    {
        gameObject.GetComponent<CharacterHorizontalMovement>().enabled = true;
    }

    public void DisableJump()
    {
        gameObject.GetComponent<CharacterJump>().enabled = false;
    }

    public void EnableJump()
    {
        gameObject.GetComponent<CharacterJump>().enabled = true;
    }

    public void DisableWeaponUse()
    {
        gameObject.GetComponent<CharacterHandleWeapon>().enabled = false;
    }

    public void EnableWeaponUse()
    {
        gameObject.GetComponent<CharacterHandleWeapon>().enabled = true;
    }

    public void OnMMEvent(PlayerEvent eventType)
    {
        if (eventType.EventType== PlayerEventTypes.ActivateDash)
        {
            ActivarDash();
        }
        if (eventType.EventType == PlayerEventTypes.ActivateWalljump)
        {
            ActivarWalljump();
        }
        if (eventType.EventType == PlayerEventTypes.ActivateDoublejump)
        {
            ActivarDoublejump();
        }
        if (eventType.EventType == PlayerEventTypes.DisableDoublejump)
        {
            DesactivarDoublejump();
        }
        if (eventType.EventType == PlayerEventTypes.ActivateBasicGun)
        {
            ActivarBasicGun();
        }
        if (eventType.EventType == PlayerEventTypes.DisableHorizontalMovement)
        {
            DisableHorizontalMovement();
        }
        if (eventType.EventType == PlayerEventTypes.EnableHorizontalMovement)
        {
            EnableHorizontalMovement();
        }
        if (eventType.EventType == PlayerEventTypes.DisableJump)
        {
            DisableJump();
        }
        if (eventType.EventType == PlayerEventTypes.EnableJump)
        {
            EnableJump();
        }
        if (eventType.EventType == PlayerEventTypes.DisableWeaponUse)
        {
            DisableWeaponUse();
        }
        if (eventType.EventType == PlayerEventTypes.EnableWeaponUse)
        {
            EnableWeaponUse();
        }
        if (eventType.EventType == PlayerEventTypes.ChangeHorizontalMovementSpeed)
        {
            ChangeHorizontalMovementSpeed(eventType.NewValue);
        }
        if (eventType.animationParameterTrigger != null)
        {
            eventType.animationParameterTrigger.Animator = gameObject.GetComponent<Animator>();
            eventType.animationParameterTrigger.Trigger();
        }
    }

    private void ChangeHorizontalMovementSpeed(float newValue)
    {
        gameObject.GetComponent<CharacterHorizontalMovement>().MovementSpeed = newValue;
    }
}
