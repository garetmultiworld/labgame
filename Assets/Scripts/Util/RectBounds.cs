﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class RectBounds: MonoBehaviour
{
    public Rect TheRect;
    public Color outline;
    public Color fill;
    public string Label;

    public Vector2 GetTopLeft()
    {
        return new Vector2(TheRect.x - (TheRect.width / 2), TheRect.y - (TheRect.height / 2));
    }

    public Vector2 GetBottomRight()
    {
        return new Vector2(TheRect.x + (TheRect.width / 2), TheRect.y + (TheRect.height / 2));
    }
}

[CustomEditor(typeof(RectBounds))]
public class RectBoundsEditor : Editor
{
    void OnSceneGUI()
    {
        var rectExample = (RectBounds)target;

        var rect = RectUtils.ResizeRect(
            rectExample.TheRect,
            Handles.CubeHandleCap,
            rectExample.outline,
            rectExample.fill,
            HandleUtility.GetHandleSize(Vector3.zero) * .1f,
            .1f);

        rectExample.TheRect = rect;
    }
}