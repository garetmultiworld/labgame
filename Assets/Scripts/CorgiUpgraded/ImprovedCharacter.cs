﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImprovedCharacter : Character
{

    public override void Flip(bool IgnoreFlipOnDirectionChange = false)
    {
        base.Flip(IgnoreFlipOnDirectionChange);
        CharacterMovementEvent.Trigger(this, CharacterMovementEventTypes.Flip);
    }

}
