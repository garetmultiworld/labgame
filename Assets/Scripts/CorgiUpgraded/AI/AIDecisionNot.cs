﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Negates the result of the selected decision
/// Meaning that if the result is true it will become false and viceversa
/// </summary>
public class AIDecisionNot : AIDecision
{
    public AIDecision decision;

    public override void Initialization()
    {
        decision.Initialization();
    }

    public override bool Decide()
    {
        return !decision.Decide();
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        decision.OnEnterState();
    }

    public override void OnExitState()
    {
        base.OnExitState();
        decision.OnExitState();
    }
}
