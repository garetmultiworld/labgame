﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Applies the XOR logical gate to the chosen decisions
/// Meaning that only one of the decisions can be true for this decision to be true
/// If Xnor is true, the result of this decision will be negated (true becomes false, false becomes true)
/// </summary>
public class AIDecisionXor : AIDecision
{

    [Reorderable(null, "Decision", null)]
    public AIDecisionList Decisions;
    public bool Xnor= false;

    public override void Initialization()
    {
        foreach (AIDecision decision in Decisions)
        {
            decision.Initialization();
        }
    }

    public override bool Decide()
    {
        bool result=false;
        bool individualResult;
        foreach (AIDecision decision in Decisions)
        {
            individualResult = decision.Decide();
            if (individualResult && !result)
            {
                result = true;
            }else if (individualResult && result)
            {
                return false;
            }
        }
        if (Xnor)
        {
            return !result;
        }
        else
        {
            return result;
        }
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        foreach (AIDecision decision in Decisions)
        {
            decision.OnEnterState();
        }
    }

    public override void OnExitState()
    {
        base.OnExitState();
        foreach (AIDecision decision in Decisions)
        {
            decision.OnExitState();
        }
    }
}
