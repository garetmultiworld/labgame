﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterFly))]
public class AIActionFlyWander : AIAction
{

    public RectBounds Bounds;

    /// the minimum distance from the target this Character can reach.
    public float MinimumDistance = 1f;

    protected CharacterFly _characterFly;
    protected Vector3 targetPosition = new Vector3();

    protected bool movingX;
    protected bool movingY;
    protected bool prevFlightState;

    protected override void Initialization()
    {
        _characterFly = this.gameObject.GetComponent<CharacterFly>();
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        prevFlightState = _characterFly.AlwaysFlying;
        _characterFly.AlwaysFlying = true;
        _characterFly.StartFlight();
        targetPosition.x = this.transform.position.x;
        targetPosition.y = this.transform.position.y;
    }

    public override void PerformAction()
    {
        Fly();
    }

    protected virtual void Fly()
    {

        CheckIfArrived();

        if (!movingX && !movingY)
        {
            Vector2 topLeft = Bounds.GetTopLeft();
            Vector2 bottomRight= Bounds.GetBottomRight();
            targetPosition.x = Random.Range(topLeft.x, bottomRight.x);
            targetPosition.y = Random.Range(topLeft.y, bottomRight.y);
        }

        CheckIfArrived();

        if (movingX)
        {
            if (this.transform.position.x < targetPosition.x)
            {
                _characterFly.SetHorizontalMove(1f);
            }
            else
            {
                _characterFly.SetHorizontalMove(-1f);
            }
        }

        if (movingY)
        {
            if (this.transform.position.y < targetPosition.y)
            {
                _characterFly.SetVerticalMove(1f);
            }
            else
            {
                _characterFly.SetVerticalMove(-1f);
            }
        }

    }

    private void CheckIfArrived()
    {
        movingX = true;
        movingY = true;
        if (Mathf.Abs(this.transform.position.x - targetPosition.x) < MinimumDistance)
        {
            movingX = false;
            _characterFly.SetHorizontalMove(0f);
        }

        if (Mathf.Abs(this.transform.position.y - targetPosition.y) < MinimumDistance)
        {
            movingY = false;
            _characterFly.SetVerticalMove(0f);
        }
    }

    public override void OnExitState()
    {
        base.OnExitState();
        _characterFly.AlwaysFlying = prevFlightState;
        if (!_characterFly.AlwaysFlying)
        {
            _characterFly.StopFlight();
        }
        _characterFly.SetHorizontalMove(0f);
        _characterFly.SetVerticalMove(0f);
    }
}
