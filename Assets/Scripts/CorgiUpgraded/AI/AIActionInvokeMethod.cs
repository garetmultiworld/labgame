﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AIActionInvokeMethod : AIAction
{

    public UnityEvent OnEnterInvocations;
    public UnityEvent OnExitInvocations;

    public override void OnEnterState()
    {
        base.OnEnterState();
        OnEnterInvocations.Invoke();
    }

    public override void PerformAction()
    {
        
    }

    public override void OnExitState()
    {
        base.OnExitState();
        OnExitInvocations.Invoke();
    }
}
