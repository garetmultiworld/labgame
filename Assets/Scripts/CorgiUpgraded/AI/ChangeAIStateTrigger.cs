﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAIStateTrigger : TriggerInterface
{

    public AIBrainInterface Brain;
    public string NewState;
    
    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        Brain.TransitionToState(NewState);
    }
}
