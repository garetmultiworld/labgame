﻿using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBrainInterface:MonoBehaviour
{
    [ReadOnly]
    /// the time we've spent in the current state
    public Transform Target;
    [ReadOnly]
    /// the current target
    public float TimeInThisState;

    public void TransitionToState(string newState)
    {
    }
}
