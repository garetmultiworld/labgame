﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AITransitionsImprovedList : ReorderableArray<AITransitionImproved>
{
}

/// <summary>
/// A State is a combination of one or more actions, and one or more transitions. An example of a state could be "_patrolling until an enemy gets in range_".
/// </summary>
[System.Serializable]
public class AIStateImproved
{
    /// the name of the state (will be used as a reference in Transitions
    public string StateName;

    [Reorderable(null, "Action", null)]
    public AIActionsList Actions;
    [Reorderable(null, "TransitionImproved", null)]
    public AITransitionsImprovedList Transitions;

    protected AIBrainImproved _brain;

    /// <summary>
    /// Sets this state's brain to the one specified in parameters
    /// </summary>
    /// <param name="brain"></param>
    public virtual void SetBrain(AIBrainImproved brain)
    {
        _brain = brain;
    }

    /// <summary>
    /// On enter state we pass that info to our actions and decisions
    /// </summary>
    public virtual void EnterState()
    {
        foreach (AIAction action in Actions)
        {
            action.OnEnterState();
        }
        foreach (AITransitionImproved transition in Transitions)
        {
            if (transition.Decision != null)
            {
                transition.Decision.OnEnterState();
            }
        }
    }

    /// <summary>
    /// On exit state we pass that info to our actions and decisions
    /// </summary>
    public virtual void ExitState()
    {
        foreach (AIAction action in Actions)
        {
            action.OnExitState();
        }
        foreach (AITransitionImproved transition in Transitions)
        {
            if (transition.Decision != null)
            {
                transition.Decision.OnExitState();
            }
        }
    }

    /// <summary>
    /// Performs this state's actions
    /// </summary>
    public virtual void PerformActions()
    {
        if (Actions.Count == 0) { return; }
        for (int i = 0; i < Actions.Count; i++)
        {
            if (Actions[i] != null)
            {
                Actions[i].PerformAction();
            }
            else
            {
                Debug.LogError("An action in " + _brain.gameObject.name + " is null.");
            }
        }
    }

    /// <summary>
    /// Tests this state's transitions
    /// </summary>
    public virtual void EvaluateTransitions()
    {
        if (Transitions.Count == 0) { return; }
        for (int i = 0; i < Transitions.Count; i++)
        {
            if (Transitions[i].Decision != null)
            {
                string newState = null;
                float chance = Random.Range(0f, 100f);
                float currentChance = 0f;
                if (Transitions[i].Decision.Decide())
                {
                    if (Transitions[i].TrueStates.Count>0)
                    {
                        for (int st = 0; st < Transitions[i].TrueStates.Count; st++)
                        {
                            if(chance<= (Transitions[i].TrueStates[st].Chances+ currentChance))
                            {
                                newState = Transitions[i].TrueStates[st].State;
                                break;
                            }
                            currentChance += Transitions[i].TrueStates[st].Chances;
                        }
                    }
                }
                else
                {
                    if (Transitions[i].FalseStates.Count > 0)
                    {
                        for (int st = 0; st < Transitions[i].FalseStates.Count; st++)
                        {
                            if (chance <= (Transitions[i].FalseStates[st].Chances + currentChance))
                            {
                                newState = Transitions[i].FalseStates[st].State;
                                break;
                            }
                            currentChance += Transitions[i].FalseStates[st].Chances;
                        }
                    }
                }
                if (newState != null)
                {
                    _brain.TransitionToState(newState);
                    break;
                }
            }
        }
    }
}