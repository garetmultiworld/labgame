﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Applies the OR logical gate to the chosen decisions
/// Meaning that if any of the decisions are true, this decision will also be true
/// If Nor is true, the result of this decision will be negated (true becomes false, false becomes true)
/// </summary>
public class AIDecisionOr : AIDecision
{

    [Reorderable(null, "Decision", null)]
    public AIDecisionList Decisions;
    public bool Nor= false;

    public override void Initialization()
    {
        foreach (AIDecision decision in Decisions)
        {
            decision.Initialization();
        }
    }

    public override bool Decide()
    {
        bool result = false;
        foreach (AIDecision decision in Decisions)
        {
            if (decision.Decide())
            {
                result=true;
                break;
            }
        }
        if (Nor)
        {
            return !result;
        }
        else
        {
            return result;
        }
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        foreach (AIDecision decision in Decisions)
        {
            decision.OnEnterState();
        }
    }

    public override void OnExitState()
    {
        base.OnExitState();
        foreach (AIDecision decision in Decisions)
        {
            decision.OnExitState();
        }
    }

}
