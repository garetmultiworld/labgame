﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIActionFlyOtherTowardsDestination : AIAction
{
    /// the minimum distance from the target this Character can reach.
    public float MinimumDistance = 1f;

    protected CharacterFly _characterFly;
    protected int _numberOfJumps = 0;
    public GameObject Other;
    public GameObject Destination;
    public bool snapPosition = false;


    /// <summary>
    /// On init we grab our CharacterFly ability
    /// </summary>
    protected override void Initialization()
    {
        _characterFly = Other.GetComponent<CharacterFly>();
    }

    /// <summary>
    /// On PerformAction we fly
    /// </summary>
    public override void PerformAction()
    {
        Fly();
    }

    /// <summary>
    /// Moves the character towards the target if needed
    /// </summary>
    protected virtual void Fly()
    {
        if (Destination == null)
        {
            return;
        }

        bool arrived = true;

        if (Other.transform.position.x < Destination.transform.position.x)
        {
            _characterFly.SetHorizontalMove(1f);
        }
        else
        {
            _characterFly.SetHorizontalMove(-1f);
        }

        if (Other.transform.position.y < Destination.transform.position.y)
        {
            _characterFly.SetVerticalMove(1f);
        }
        else
        {
            _characterFly.SetVerticalMove(-1f);
        }

        if (Mathf.Abs(Other.transform.position.x - Destination.transform.position.x) < MinimumDistance)
        {
            _characterFly.SetHorizontalMove(0f);
        }
        else
        {
            arrived = false;
        }

        if (Mathf.Abs(Other.transform.position.y - Destination.transform.position.y) < MinimumDistance)
        {
            _characterFly.SetVerticalMove(0f);
        }
        else
        {
            arrived = false;
        }

        if (arrived&&snapPosition)
        {
            Other.transform.position = Destination.transform.position;
        }
    }

    /// <summary>
    /// On exit state we stop our movement
    /// </summary>
    public override void OnExitState()
    {
        base.OnExitState();

        _characterFly.SetHorizontalMove(0f);
        _characterFly.SetVerticalMove(0f);
    }
}
