﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDecisionOtherIsAlive : AIDecision
{

    public Character Other;

    /// <summary>
    /// On Decide we check whether the Target is facing us
    /// </summary>
    /// <returns></returns>
    public override bool Decide()
    {
        return CheckIfOtherIsAlive();
    }

    /// <summary>
    /// Returns true if the Brain's Target is facing us (this will require that the Target has a Character component)
    /// </summary>
    /// <returns></returns>
    protected virtual bool CheckIfOtherIsAlive()
    {
        if (_brain.Target == null)
        {
            return false;
        }

        if (Other != null)
        {
            if (Other.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        return false;
    }
}
