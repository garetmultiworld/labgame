﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterFly))]
public class AIActionFlyTowardsTargetLastPosition : AIAction
{
    /// the minimum distance from the target this Character can reach.
    public float MinimumDistance = 1f;

    protected CharacterFly _characterFly;
    protected Vector3 targetPosition=new Vector3();

    protected bool movingX;
    protected bool movingY;
    protected bool prevFlightState;
    protected GameObject movable;

    protected override void Initialization()
    {
        _characterFly = this.gameObject.GetComponent<CharacterFly>();
        movable = gameObject;
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        prevFlightState = _characterFly.AlwaysFlying;
        _characterFly.AlwaysFlying = true;
        _characterFly.StartFlight();
        targetPosition.x = movable.transform.position.x;
        targetPosition.y = movable.transform.position.y;
    }

    public override void PerformAction()
    {
        Fly();
    }

    protected virtual void Fly()
    {
        if (_brain.Target == null)
        {
            return;
        }

        CheckIfArrived();

        if (!movingX && !movingY)
        {
            targetPosition.x = _brain.Target.position.x;
            targetPosition.y = _brain.Target.position.y;
        }

        CheckIfArrived();

        if (movingX)
        {
            if (movable.transform.position.x < targetPosition.x)
            {
                _characterFly.SetHorizontalMove(1f);
            }
            else
            {
                _characterFly.SetHorizontalMove(-1f);
            }
        }

        if (movingY)
        {
            if (movable.transform.position.y < targetPosition.y)
            {
                _characterFly.SetVerticalMove(1f);
            }
            else
            {
                _characterFly.SetVerticalMove(-1f);
            }
        }
        
    }

    private void CheckIfArrived()
    {
        movingX = true;
        movingY = true;
        if (Mathf.Abs(movable.transform.position.x - targetPosition.x) < MinimumDistance)
        {
            movingX = false;
            _characterFly.SetHorizontalMove(0f);
        }

        if (Mathf.Abs(movable.transform.position.y - targetPosition.y) < MinimumDistance)
        {
            movingY = false;
            _characterFly.SetVerticalMove(0f);
        }
    }

    public override void OnExitState()
    {
        base.OnExitState();
        _characterFly.AlwaysFlying = prevFlightState;
        if (!_characterFly.AlwaysFlying)
        {
            _characterFly.StopFlight();
        }
        _characterFly.SetHorizontalMove(0f);
        _characterFly.SetVerticalMove(0f);
    }

}
