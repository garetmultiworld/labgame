﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIStateChoice
{

    public string State;
    [Range(0f, 100f)]
    public float Chances=100f;

}
