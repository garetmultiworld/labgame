﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGColorHealth : MonoBehaviour, MMEventListener<MMDamageTakenEvent>
{

    public SpriteRenderer[] backgrounds;

    public Color [] colors;

    private Gradient gradient;
    private GradientColorKey[] colorKey;
    private GradientAlphaKey[] alphaKey;

    public void OnMMEvent(MMDamageTakenEvent damageTaken)
    {
        if (damageTaken.AffectedCharacter == null)
        {
            return;
        }
        if (damageTaken.AffectedCharacter.CharacterType ==Character.CharacterTypes.Player)
        {
            Health health=damageTaken.AffectedCharacter.GetComponent<Health>();
            Color color = gradient.Evaluate(((float)health.CurrentHealth) / ((float)health.MaximumHealth));
            for (int i = 0; i < backgrounds.Length; i++)
            {
                backgrounds[i].color= color;
            }
        }
    }

    protected virtual void Start()
    {
        gradient = new Gradient();
        colorKey = new GradientColorKey[colors.Length];
        alphaKey = new GradientAlphaKey[colors.Length];
        float step = 1f / (colors.Length - 1);
        float currentStep = 0.0f;
        for (int i = 0; i < colors.Length; i++)
        {
            colorKey[i].color = colors[i];
            colorKey[i].time = currentStep;
            alphaKey[i].alpha = 1.0f;
            alphaKey[i].time = currentStep;
            currentStep += step;
        }
        gradient.SetKeys(colorKey, alphaKey);
        Color color = gradient.Evaluate(1.0f);
        for (int i = 0; i < backgrounds.Length; i++)
        {
            backgrounds[i].color = color;
        }
        MMEventManager.AddListener<MMDamageTakenEvent>(this);
    }

}
