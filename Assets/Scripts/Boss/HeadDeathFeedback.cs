﻿using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadDeathFeedback : MMFeedback
{

    public string Head;
    public FirstBoss boss;
    protected AIBrainImproved _brain;
    protected List<AITransitionImproved> _transitionsTrue=new List<AITransitionImproved>();
    protected List<AITransitionImproved> _transitionsFalse = new List<AITransitionImproved>();

    protected override void CustomPlayFeedback(Vector3 position, float attenuation = 1)
    {
        foreach (AITransitionImproved transition in _transitionsTrue)
        {
            DistributeChances(transition.TrueStates);
        }
        foreach (AITransitionImproved transition in _transitionsTrue)
        {
            DistributeChances(transition.FalseStates);
        }
        boss.HeadDeath();
    }

    private void DistributeChances(AIStatesList states)
    {
        float distribution=0;
        foreach (AIStateChoice newState in states)
        {
            if (newState.State.Equals(Head + "HeadAttack"))
            {
                distribution = newState.Chances / (states.Count - 1);
                break;
            }
        }
        foreach (AIStateChoice newState in states)
        {
            if (!newState.State.Equals(Head + "HeadAttack"))
            {
                newState.Chances += distribution;
            }
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        _brain = boss.gameObject.GetComponent<AIBrainImproved>();
        foreach(AIStateImproved state in _brain.States)
        {
            foreach(AITransitionImproved transition in state.Transitions)
            {
                if (transition.TrueStates.Count > 1)
                {
                    foreach (AIStateChoice newState in transition.TrueStates)
                    {
                        if (newState.State.Equals(Head + "HeadAttack"))
                        {
                            _transitionsTrue.Add(transition);
                            break;
                        }
                    }
                }
                if (transition.FalseStates.Count > 1) 
                {
                    foreach (AIStateChoice newState in transition.FalseStates)
                    {
                        if (newState.State.Equals(Head + "HeadAttack"))
                        {
                            _transitionsFalse.Add(transition);
                            break;
                        }
                    }
                }
            }
        }
    }

}
