﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstBoss : MonoBehaviour
{

    public Health health;
    
    protected int AliveHeads=3;

    public void HeadDeath()
    {
        AliveHeads--;
        if (AliveHeads == 0)
        {
            health.Invulnerable=false;
        }
    }

}
