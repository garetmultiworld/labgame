﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class SceneChangeTrigger : TriggerInterface
{

    public LevelManager levelManager;
    public string SceneToLoad;
    public string NextStartingPoint;

    public override void Cancel()
    {}

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        InterSceneManager.Instance.NextStartingPoint = NextStartingPoint;
        InterSceneManager.Instance.NextScene = SceneToLoad;
        levelManager.GotoLevel(SceneToLoad);
    }
}
