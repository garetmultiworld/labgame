﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTrigger : TriggerInterface
{

    public GameObject Prefab;

    public override void Cancel()
    {}

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Instantiate(Prefab, new Vector3(0, 0, 0), Quaternion.identity);
    }

}
