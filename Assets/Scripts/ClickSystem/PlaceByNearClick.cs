﻿using MoreMountains.InventoryEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceByNearClick : MonoBehaviour
{

    public string NameToPlace;
    CircleCollider2D _playerRadius=null;
    [HideInInspector]
    public InventorySlot selectedSlot = null;
    [HideInInspector]
    public InventoryItem selectedItem=null;


    void Start()
    {
        _playerRadius = GetComponent<CircleCollider2D>();
    }

    void OnMouseDown()
    {
        if (_playerRadius != null)
        {
            Vector2 center = new Vector2(_playerRadius.transform.position.x, _playerRadius.transform.position.y) + _playerRadius.offset;

            Collider2D player = Physics2D.OverlapCircle(center, PlayerConfig.GetInstance().EnvironmentDetectionRadius, 1 << LayerMask.NameToLayer("Player"));
            Collider2D slot = Physics2D.OverlapCircle(center, 0.5f, 1 << LayerMask.NameToLayer("Puzzle"));

            if (player != null && slot != null && slot.gameObject.name== NameToPlace)
            {
                ItemReceiver itemReceiver = slot.gameObject.GetComponent<ItemReceiver>();
                GetComponent<FollowMouse>().enabled = false;
                transform.SetParent(slot.gameObject.transform);
                transform.localPosition = Vector3.zero;
                selectedItem.Prefab = null;
                selectedSlot.Drop();
                selectedSlot = null;
                selectedItem = null;
                if (itemReceiver&& itemReceiver.trigger)
                {
                    itemReceiver.trigger.Trigger();
                }
                enabled = false;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        
    }

}
