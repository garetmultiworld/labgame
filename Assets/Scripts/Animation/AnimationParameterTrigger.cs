﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationParameterTrigger : TriggerInterface
{

    public enum paramTypes
    {
        Float,
        Integer,
        Bool,
        Trigger
    }

    public Animator Animator;
    public paramTypes Type;
    public string ParameterName;
    public float FloatValue;
    public int IntegerValue;
    public bool BoolValue;

    public override void Cancel()
    {}

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        switch (Type)
        {
            case paramTypes.Float:
                Animator.SetFloat(ParameterName, FloatValue);
                break;
            case paramTypes.Integer:
                Animator.SetInteger(ParameterName, IntegerValue);
                break;
            case paramTypes.Bool:
                Animator.SetBool(ParameterName, BoolValue);
                break;
            case paramTypes.Trigger:
                Animator.SetTrigger(ParameterName);
                break;
        }
    }

}
