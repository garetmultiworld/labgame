﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceTrigger : TriggerInterface
{
    public TriggerInterface TriggerToFire;
    [Range(0f, 100f)]
    public float Chances = 100f;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (Random.Range(0f, 100f) <= Chances)
        {
            TriggerToFire.Trigger();
        }
    }

    public override void Cancel()
    {
    }
}
