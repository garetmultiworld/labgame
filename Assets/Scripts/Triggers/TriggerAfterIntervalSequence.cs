﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAfterIntervalSequence : TriggerInterface
{
    public TriggerInterface TheTrigger;
    public int TimesToActivate;
    public float Interval;

    protected float CurrentTime=0;
    protected int CurrentTimes=0;

    public override void Cancel()
    {
        CurrentTimes = 0;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        CurrentTimes++;
    }

    void Update()
    {
        CurrentTime += Time.deltaTime;
        if (CurrentTime <= Interval)
        {
            if (CurrentTimes >= TimesToActivate)
            {
                TheTrigger.Trigger();
            }
            CurrentTime = 0;
            CurrentTimes = 0;
        }
    }
}
