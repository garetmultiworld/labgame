﻿using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackTrigger : MMFeedback
{

    public TriggerInterface Trigger;

    protected override void CustomPlayFeedback(Vector3 position, float attenuation = 1)
    {
        Trigger.Trigger();
    }
}
