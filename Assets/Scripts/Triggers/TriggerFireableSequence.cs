﻿using System;

class TriggerFireableSequence : TriggerInterface
{
    public TriggerFireableSequenceItem[] Triggers;
    public bool loop;

    protected int index=-1;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        FireNext();
    }

    public void FireNext()
    {
        index++;
        if (index >= Triggers.Length)
        {
            if (loop)
            {
                index = 0;
            }
            else
            {
                return;
            }
        }
        Triggers[index].Trigger();
    }

}
