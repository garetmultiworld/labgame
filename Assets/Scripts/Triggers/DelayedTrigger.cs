﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedTrigger : TriggerInterface
{

    public TriggerInterface TriggerToFire;
    public float Interval;
    public bool Loop;
    public int Times=1;

    protected bool IsPlaying = false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        StartCoroutine(DelayedFire());
    }

    private IEnumerator DelayedFire()
    {
        float count = 0;
        float time = 0;
        for (count = 0; (Loop || count < Times) && IsPlaying; count++)
        {
            for (time = 0; time < Interval && IsPlaying; time += Time.deltaTime)
            {
                yield return null;
            }
            if (IsPlaying)
            {
                TriggerToFire.Trigger();
            }
        }
    }

}
