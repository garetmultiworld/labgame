﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TriggerSequenceItem
{

    public float TimeBefore;
    public TriggerInterface TriggerToFire;
    public float TimeAfter;

    public void Trigger()
    {
        TriggerToFire.Trigger();
    }

}
