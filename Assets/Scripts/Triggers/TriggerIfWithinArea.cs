﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerIfWithinArea : TriggerInterface
{

    public LayerMask FilterLayer;
    public int Quantity = 1;
    public TriggerInterface TriggerIfWithin;
    public TriggerInterface TriggerIfNotWithin;
    public bool CheckOnEnterAndExit=true;

    protected int _numInCollider=0;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            _numInCollider++;
        }
        Trigger();
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (((1 << c.gameObject.layer) & FilterLayer) != 0)
        {
            _numInCollider--;
        }
        Trigger();
    }

    public override void Cancel()
    {
        if (_numInCollider>=Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Cancel();
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Cancel();
            }
        }
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (_numInCollider >= Quantity)
        {
            if (TriggerIfWithin != null)
            {
                TriggerIfWithin.Trigger();
            }
        }
        else
        {
            if (TriggerIfNotWithin != null)
            {
                TriggerIfNotWithin.Trigger();
            }
        }
    }

}
