﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour
{

    [Tooltip("The trigger fired when the character enters")]
    public TriggerInterface OnEnter;
    [Tooltip("If the enter and exit trigger can't exist at the same time")]
    public bool EnterAndExitExclusive = false;
    [Tooltip("The trigger fired when the character exits")]
    public TriggerInterface OnExit;
    public LayerMask FilterLayer = LayerMask.NameToLayer("Player");

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (OnEnter!=null && (((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            if (EnterAndExitExclusive)
            {
                OnExit.Cancel();
            }
            OnEnter.Trigger();
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (OnExit != null && (((1 << c.gameObject.layer) & FilterLayer) != 0))
        {
            if (EnterAndExitExclusive)
            {
                OnEnter.Cancel();
            }
            OnExit.Trigger();
        }
    }

}
