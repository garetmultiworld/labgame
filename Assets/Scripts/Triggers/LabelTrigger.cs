﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabelTrigger : MonoBehaviour
{

    private TriggerInterface[] Triggers;
    
    void Start()
    {
        Triggers=this.gameObject.GetComponents<TriggerInterface>();
    }

    public void Trigger(string label)
    {
        foreach (TriggerInterface trigger in Triggers)
        {
            if (label.Equals(trigger.GetLabel()))
            {
                trigger.Trigger();
            }
        }
    }

}
