﻿using MoreMountains.InventoryEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemCheckerTrigger : MonoBehaviour
{
    public string InventoryName;
    public string ItemID;
    public int Quantity=1;
    public TriggerInterface Trigger;
}
