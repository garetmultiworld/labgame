﻿using MoreMountains.InventoryEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerIfItemInInventory : TriggerInterface
{

    public string InventoryName;
    public string ItemID;
    public int Quantity = 1;
    public TriggerInterface TriggerIfExists;
    public TriggerInterface TriggerIfNotExists;

    protected Inventory _inventory;

    void Awake()
    {
        _inventory =GameObject.Find(InventoryName).GetComponent<Inventory>();
    }

    public override void Cancel()
    {
        if(_inventory.HasItem(ItemID, Quantity))
        {
            if (TriggerIfExists != null)
            {
                TriggerIfExists.Cancel();
            }
        }
        else
        {
            if (TriggerIfNotExists != null)
            {
                TriggerIfNotExists.Cancel();
            }
        }
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (_inventory.HasItem(ItemID, Quantity))
        {
            if (TriggerIfExists != null)
            {
                TriggerIfExists.Trigger();
            }
        }
        else
        {
            if (TriggerIfNotExists != null)
            {
                TriggerIfNotExists.Trigger();
            }
        }
    }

}
