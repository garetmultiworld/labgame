﻿using MoreMountains.InventoryEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemChecker : MonoBehaviour, MMEventListener<MMInventoryEvent>
{

    public InventoryItemCheckerTrigger []Items;

    public void OnMMEvent(MMInventoryEvent eventType)
    {
        if (eventType.InventoryEventType == MMInventoryEventType.ContentChanged)
        {
            if (eventType.EventItem != null && eventType.Quantity != 0)
            {
                foreach(InventoryItemCheckerTrigger Item in Items)
                {
                    if (
                        Item.ItemID.Equals(eventType.EventItem.ItemID)&&
                        Item.InventoryName.Equals(eventType.EventItem.TargetInventoryName) &&
                        Item.Quantity==eventType.EventItem.Quantity
                        )
                    {
                        Item.Trigger.Trigger();
                    }
                }
            }
        }
    }

    void Start()
    {
        MMEventManager.AddListener<MMInventoryEvent>(this);
    }
}
