﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class I18nText : MonoBehaviour
{

    public string I18nFolder;
    public string I18nName;
    protected Text text;

    void Start()
    {
        text = GetComponent<Text>();
        text.text = I18nManager.Instance.GetFolderItem(I18nFolder,I18nName);
    }

}
