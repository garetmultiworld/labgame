﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.Tools
{
    /// <summary>
    /// Actions are behaviours and describe what your character is doing. Examples include patrolling, shooting, jumping, etc. 
    /// </summary>
    public abstract class AIAction : MonoBehaviour
    {
        public string Label;
        public abstract void PerformAction();
        public bool ActionInProgress { get; set; }
        protected AIBrainInterface _brain;
        public string AnimationTrigger="";
        public string AnimationTriggerOnExit = "";
        public string AnimationBool="";
        public bool AnimationBoolValue;
        public bool SwitchAnimationBoolOnExit;
        public Animator _animator;

        /// <summary>
        /// On Start we trigger our init method
        /// </summary>
        protected virtual void Start()
        {
            _brain = this.gameObject.GetComponent<AIBrain>();
            if (_brain == null)
            {
                _brain = this.gameObject.GetComponent<AIBrainImproved>();
            }
            Initialization();
        }

        /// <summary>
        /// Initializes the action. Meant to be overridden
        /// </summary>
        protected virtual void Initialization()
        {

        }

        /// <summary>
        /// Describes what happens when the brain enters the state this action is in. Meant to be overridden.
        /// </summary>
        public virtual void OnEnterState()
        {
            ActionInProgress = true;
            if (!AnimationTrigger.Equals(""))
            {
                _animator.SetTrigger(AnimationTrigger);
            }
            if (!AnimationBool.Equals(""))
            {
                _animator.SetBool(AnimationBool,AnimationBoolValue);
            }
        }

        /// <summary>
        /// Describes what happens when the brain exits the state this action is in. Meant to be overridden.
        /// </summary>
        public virtual void OnExitState()
        {
            ActionInProgress = false;
            if (!AnimationTriggerOnExit.Equals(""))
            {
                _animator.SetTrigger(AnimationTriggerOnExit);
            }
            if (!AnimationBool.Equals("")&& SwitchAnimationBoolOnExit)
            {
                _animator.SetBool(AnimationBool, !AnimationBoolValue);
            }
        }
    }
}