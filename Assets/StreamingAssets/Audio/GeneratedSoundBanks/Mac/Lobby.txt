Event	ID	Name			Wwise Object Path	Notes
	2018945488	Arealoop			\Default Work Unit\Arealoop	
	2204642885	Reactor			\Default Work Unit\Reactor	
	2454616260	Sword			\Default Work Unit\Sword	
	2698255118	Evil_Door			\Default Work Unit\Evil_Door	
	3376857143	Teslavoz			\Default Work Unit\Teslavoz	
	3831537568	Puerta			\Default Work Unit\Puerta	
	4062477502	wobble			\Default Work Unit\wobble	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	11685703	Tesla 2	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\tesla sfx 1_9B1CB5A7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cola\Tesla 2		67472
	45066868	synth 3 69bpm_8m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\synth 3 69bpm_8m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\synth 3 69bpm_8m		5354500
	58401874	Tesla 4	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\tesla sfx 2_9C034FF6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cola\Tesla 4		53828
	65923476	Harp	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Audio Files\Harp_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area Loop Lobby\Audio Files\Harp		3801232
	112130193	Tesla 2	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\tesla sfx 1_EC965646.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cabeza\Tesla 2		74188
	160533147	Tesla 3	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\tesla sfx 1.2_25D073C7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cabeza\Tesla 3		57480
	197541756	cello 69bpm-8m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\cello 69bpm-8m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\cello 69bpm-8m		5342848
	212358851	wobble	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\health wooble_B79D5F3F.wem		\Actor-Mixer Hierarchy\Default Work Unit\wobble\wobble		65192
	213590551	Piano	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Audio Files\Piano_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area Loop Lobby\Audio Files\Piano		3014800
	230206708	Tesla 1	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\R2D2 sx 3_F077C0EC.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cola\Tesla 1		134932
	263164401	sword	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\espada v.1_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\sword		56004
	264565521	wobble 2	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\health wooble_5327C99A.wem		\Actor-Mixer Hierarchy\Default Work Unit\wobble\wobble 2		73930
	298264355	Tesla 3	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\tesla sfx 1.2_38A75D69.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cola\Tesla 3		36140
	298471741	cello 69bpm-8m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\Vl 2 Theme 69bpm 8m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\cello 69bpm-8m		5342676
	395597903	Choir	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Audio Files\Choir _D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area Loop Lobby\Audio Files\Choir		3276944
	408500221	Db theme 69bpm-8m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\Db theme 69bpm-8m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\Db theme 69bpm-8m		5342848
	429963594	golpe puerta nada	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\golpe puerta_8BD5B62D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta\golpe puerta nada		24060
	444545996	String Scary 69bpm-5m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\String Scary 69bpm-5m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\String Scary 69bpm-5m		3340068
	449041089	Violin 1	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Audio Files\Violin 1_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area Loop Lobby\Audio Files\Violin 1		3145872
	468447106	Viola 69 bpm 8m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\Viola 69 bpm 8m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\Viola 69 bpm 8m		5348636
	475849940	Tesla 4	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\tesla sfx 2_22D90188.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cabeza\Tesla 4		227964
	499761366	synth 3 69bpm_8m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\synth 3v.2 69bpm-m8_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\synth 3 69bpm_8m		5342848
	533456125	String Scary 69bpm-5m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\Strings Fx 2 69bpm-8m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\String Scary 69bpm-5m		5342848
	700242353	Reactor	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\reactor _8D5F62B1.wem		\Actor-Mixer Hierarchy\Default Work Unit\Reactor		1598948
	714808531	cello 69bpm-8m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\Vl 1 Theme69bpm 8m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\cello 69bpm-8m		5342676
	754660758	Cello	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Audio Files\Cello _D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area Loop Lobby\Audio Files\Cello		3145872
	754876284	synth  69 bpm-16m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\synth  69 bpm-16m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\synth  69 bpm-16m		10685776
	814518152	String Scary 69bpm-5m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\Strings fx 1 69 bpm-8m _D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\String Scary 69bpm-5m		5342848
	822812900	golpe puerta	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\golpe puerta_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta\golpe puerta		262288
	907747040	vilins 2 fast	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Audio Files\vilins 2 fast _D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area Loop Lobby\Audio Files\vilins 2 fast		3276944
	942637717	Tesla 1	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\R2D2 sx 3_21A5BC4D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Tesla voz\Tesla Voz Cabeza\Tesla 1		131216
	989709873	bells	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Audio Files\bells _D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area Loop Lobby\Audio Files\bells		3145872
	1039445058	Watherphone 69bpm-16m	Y:\Documents\labgame copy_WwiseProject\.cache\Mac\SFX\Evil Area Loop\Watherphone 69bpm-16m_D79FB2B7.wem		\Interactive Music Hierarchy\Area loops\Area loop Evil Lobby\Evil Area Loop\Watherphone 69bpm-16m		10685504

